//: Playground - noun: a place where people can play

import UIKit

// Utilisation de protocol + protocol extension
class EventReceiver {
    // This method's signature must not change
    func receiveEvents(events: [[String: AnyObject]]) {
        // Process events...
        NSNotificationCenter.defaultCenter().postNotificationName(
            "EventReceiverDidReceiveEvents", object: self, userInfo: ["events": events])
    }
}

// Displays a list of all events received, stored in `receivedEvents`
class MyTableViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var events: [Event] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserverForName(
            "EventReceiverDidReceiveEvents",
            object: nil, queue: nil) { [weak self] notification in
                guard let strongSelf = self,
                    let eventData = notification.userInfo?["events"] as? [[String: AnyObject]],
                    let events = strongSelf.parseEvents(eventData)
                    else {
                        return
                }
                self?.events += events
                self?.tableView.reloadData()
        }
    }
    
    func parseEvents(events: [[String: AnyObject]]) -> [Event]? {
        // TODO
        return nil
    }
    
    
    // DataSource methods...
}


struct Event {
    var uid: String
    var timestamp: NSDate
    var data: [String: AnyObject]
}


class MyDetailViewController: UIViewController {
    var event: Event!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserverForName(
        "EventsReceiverDidReceiveEvents", object: nil, queue: nil) { [weak self] notification in
            guard let strongSelf = self,
                let eventData = notification.userInfo?["events"] as? [[String: AnyObject]],
                let events = strongSelf.parseEvents(eventData),
                let event = events.filter({ $0.uid == strongSelf.event.uid }).first
                    else {
                        return
                }
            strongSelf.event.timestamp = event.timestamp
            strongSelf.event.data = event.data
        }
    }
    
    func parseEvents(events: [[String: AnyObject]]) -> [Event]? {
        // TODO
        return nil
    }
}


// 1. The view controllers will still receive events after they have been deallocated. Fix that
// 2. Write the parseEvent method
// 3. Refactor the code using an EventNotifier protocol to deduplicate the code in viewDidLoad
protocol EventNotifier {
}

extension EventNotifier {
    func addEventReceivedObserver(block: [Event] -> ()) -> AnyObject {
        // TODO: Parse the events into Event structs and add a NSNotification observer. Returns the NSNotification token (result of notificationCenter.addObserverForName(...)
        return ""
    }
}
// 4. Refactor MyTableViewController to merge the newly received events with its `events` property, sorting them by date (you can also change the type of events to something else than a list if you want, the tableView's dataSource methods should just have access to a sorted view of them)
