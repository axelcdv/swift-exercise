# Swift exercise

Open the file with XCode.

Instructions:
1. The view controllers will still receive events after they have been
deallocated. Fix that
2. Write the parseEvent method
3. Refactor the code using an EventNotifier protocol to deduplicate the code
in viewDidLoad
4. Refactor MyTableViewController to merge the newly received events with its
events property, sorting them by date (you can also change the type of events
to something else than a list if you want, the tableView's dataSource methods
should just have access to a sorted view of them)

